		<rewardlist type="RATED_GROUPED">
			<group chance="100.0">
				<reward item_id="13458" min="1" max="1" chance="0.005"/>
				<reward item_id="13467" min="1" max="1" chance="0.005"/>
			</group>
			<group chance="100.0">
				<reward item_id="57" min="3471" max="8089" chance="70"/>
				<reward item_id="15640" min="1" max="1" chance="0.5"/>
				<reward item_id="14166" min="1" max="1" chance="0.5"/>
				<reward item_id="14167" min="1" max="1" chance="0.1"/>
				<reward item_id="9626" min="1" max="1" chance="0.05"/>
				<reward item_id="9548" min="1" max="1" chance="0.05"/>
				<reward item_id="9554" min="1" max="1" chance="0.01"/>
				<reward item_id="14168" min="1" max="1" chance="0.01"/>
				<reward item_id="14169" min="1" max="1" chance="0.005"/>
				<reward item_id="959" min="1" max="1" chance="0.005"/>
				<reward item_id="15815" min="1" max="1" chance="0.005"/>
			</group>
		</rewardlist>
		<rewardlist type="SWEEP">
			<reward item_id="36532" min="1" max="1" chance="25.47"/>
			<reward item_id="36533" min="1" max="1" chance="4.985"/>
			<reward item_id="36882" min="1" max="1" chance="0.1"/>
			<reward item_id="36881" min="1" max="1" chance="0.05"/>
			<reward item_id="36884" min="1" max="1" chance="0.05"/>
			<reward item_id="36883" min="1" max="1" chance="0.005"/>
		</rewardlist>
